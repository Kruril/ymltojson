#!/usr/bin/env node


const fs = require("fs");
const yaml = require('./src/YAML/Yaml');
const {toJSON} = require("yaml/util");
(function () {
    "use strict";

    const file = process.argv[2]

    fs.readFile(file, 'utf8', (err, jsonString) => {
        let filename = file.split("\\").reverse().at(0)
        console.log(filename)
        if (err !== null) {
            return err
        }
        if (filename.endsWith("json")) {
            //TODO : to Yaml
            console.log(yaml.stringify(jsonString))

        }
        else if (filename.endsWith("yml") || filename.endsWith("yaml")) {
            //TODO : to json
            console.log(toJSON(jsonString))
        } else {
            err.code = 900
            err.name = "Unknown extension"
            err.message = "unknown extension actual : {" + filename.split("\.") + "}. Expected  {'json','yml','yaml'}"
            return err
        }
    })


    const test = {
        "a": "a",
        "b": {
            "c": "c",
            "d": ["e", "f", "g"]
        }
    }
    // console.log(JSON.parse(json))
    // console.log(YAML.stringify(json))

}());