const ToYaml = require("./ToYaml");

class Yaml {
    stringify(data, config) {
        return (new ToYaml(config))[typeof data](data)
    }
}

module.exports = new Yaml();